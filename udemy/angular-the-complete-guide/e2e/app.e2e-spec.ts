import { AngularTheCompleteGuidePage } from './app.po';

describe('angular-the-complete-guide App', () => {
  let page: AngularTheCompleteGuidePage;

  beforeEach(() => {
    page = new AngularTheCompleteGuidePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
