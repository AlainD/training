import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './shared/layout/layout.component';
import { HomeComponent } from './home/home.component';
import { Exercise1Component } from './section2/exercise1/exercise1.component';

const routes: Routes = [
  // Main redirection
  {path: '', redirectTo: 'home', pathMatch: 'full'},

  // App views
  {
    path: '', component: LayoutComponent,
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'section2',
        children: [
          { path: 'exercise1', component: Exercise1Component }
        ]
      }
    ]
  },

  // Handle all other routes
  {path: '**',  redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
