import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Exercise1Module } from './exercise1/exercise1.module';

@NgModule({
  imports: [
    CommonModule,
    Exercise1Module
  ],
  declarations: []
})
export class Section2Module { }
