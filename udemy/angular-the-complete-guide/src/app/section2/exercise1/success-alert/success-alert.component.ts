import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-success-alert',
  templateUrl: './success-alert.component.html',
  styleUrls: ['./success-alert.component.scss']
})
export class SuccessAlertComponent implements OnInit {
  public message: string;

  constructor() { }

  ngOnInit() {
    this.message = 'I am a success alert';
  }

}
