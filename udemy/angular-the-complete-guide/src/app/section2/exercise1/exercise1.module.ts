import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Exercise1Component } from './exercise1.component';
import { WarningAlertComponent } from './warning-alert/warning-alert.component';
import { SuccessAlertComponent } from './success-alert/success-alert.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [Exercise1Component, WarningAlertComponent, SuccessAlertComponent]
})
export class Exercise1Module { }
