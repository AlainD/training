import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-warning-alert',
  templateUrl: './warning-alert.component.html',
  styleUrls: ['./warning-alert.component.scss']
})
export class WarningAlertComponent implements OnInit {
  public message: string;

  constructor() { }

  ngOnInit() {
    this.message = 'I am a warning alert!';
  }

}
