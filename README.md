# TRAINING

This repo is intended to list some training sessions I follow (on Udemy, Thinkster, other platforms out there)

## UDEMY

* [Angular - The Complete Guide](./udemy/angular-the-complete-guide) (by Maximillian Schwarzmüller)

## OTHER

* [Test on ngx-charts](./test-ngx-charts)
