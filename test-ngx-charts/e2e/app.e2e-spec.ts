import { TestNgxChartsPage } from './app.po';

describe('test-ngx-charts App', () => {
  let page: TestNgxChartsPage;

  beforeEach(() => {
    page = new TestNgxChartsPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
