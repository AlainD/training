import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class VBarChartService {

  constructor(private http: Http) { }

  public getData(): Observable<any[]> {
    return this.http.get('./assets/api/data.json')
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  public handleError(error: Response | any) {
    let errorMessage: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      // errorMessage = `${error.status} - ${error.statusText || ''} ${err}`;
      errorMessage = `${err}`;
    } else {
      errorMessage = error.message ? error.message : error.toString();
    }
    return Observable.throw(errorMessage);
  }

}
