import { Component, OnInit } from '@angular/core';
import { VBarChartService } from './v-bar-chart.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/share';

@Component({
  selector: 'app-v-bar-chart',
  templateUrl: './v-bar-chart.component.html',
  styleUrls: ['./v-bar-chart.component.scss']
})
export class VBarChartComponent implements OnInit {
  single$: Observable<any[]>;
  view: number[] = [700, 400];
  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  constructor(private vBarChartService: VBarChartService) { }

  ngOnInit() {
    this.initChart();
  }

  initChart(): void {
    this.single$ = this.vBarChartService.getData().share();
  }

  onSelect(event: any): void {
    console.log(event);
  }

}
