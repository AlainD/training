import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { VBarChartComponent } from './v-bar-chart/v-bar-chart.component';
import { VBarChartService } from './v-bar-chart/v-bar-chart.service';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    NgxChartsModule
  ],
  declarations: [
    HomeComponent,
    VBarChartComponent
  ],
  providers: [
    VBarChartService
  ]
})
export class HomeModule { }
