import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  // Main redirection
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  // App views
  { path: 'home', loadChildren: 'app/home/home.module#HomeModule' },
  // Handle all other routes
  { path: '**',  redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
